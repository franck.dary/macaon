#! /bin/bash

rm -rf AppDir && cp -r ../AppDirTemplate AppDir && cmake .. -DCMAKE_INSTALL_PREFIX=/usr && make -j && make install DESTDIR=AppDir && linuxdeploy --appdir AppDir -d AppDir/macaon.desktop -i AppDir/macaon.svg --output appimage -l $(ldd AppDir/usr/bin/macaon | grep "stdc++" | awk '{print $3}')

