#include "Beam.hpp"

Beam::Beam(std::size_t width, float threshold, BaseConfig & model, const ReadingMachine & machine) : width(width), threshold(threshold)
{
  model.setStrategy(machine.getStrategyDefinition());
  model.addPredicted(machine.getPredicted());
  model.setState(model.getStrategy().getInitialState());
  elements.emplace_back(model);
}

Beam::Element::Element(const Element & other, int nextTransition) : Element(other)
{
  this->nextTransition = nextTransition;
}

Beam::Element::Element(const BaseConfig & model) : config(model)
{
}

Beam::Element & Beam::operator[](std::size_t index)
{
  return elements[index];
}

void Beam::update(ReadingMachine & machine, bool debug)
{
  ended = true;
  auto currentNbElements = elements.size();

  if (debug)
    fmt::print(stderr, "{:*<{}}BEAM START{:*<{}}\n", "", 37, "", 36);

  for (unsigned int index = 0; index < currentNbElements; index++)
  {
    if (elements[index].ended)
      continue;

    ended = false;

    auto & classifier = *machine.getClassifier(elements[index].config.getState());

    if (machine.hasSplitWordTransitionSet())
      elements[index].config.setAppliableSplitTransitions(machine.getSplitWordTransitionSet().getNAppliableTransitions(elements[index].config, Config::maxNbAppliableSplitTransitions));

    auto appliableTransitions = machine.getTransitionSet(elements[index].config.getState()).getAppliableTransitions(elements[index].config);
    elements[index].config.setAppliableTransitions(appliableTransitions);

    auto neuralInput = classifier.getNN()->extractContext(elements[index].config);

    auto prediction = classifier.isRegression() ? classifier.getNN()->forward(neuralInput, elements[index].config.getState()).squeeze(0) : torch::softmax(classifier.getNN()->forward(neuralInput, elements[index].config.getState()).squeeze(0), 0);
    float entropy = classifier.isRegression() ? 0.0 : NeuralNetworkImpl::entropy(prediction);
    std::vector<std::pair<float, int>> scoresOfTransitions;
    for (unsigned int i = 0; i < prediction.size(0); i++)
    {
      float score = prediction[i].item<float>();
      if (appliableTransitions[i])
        scoresOfTransitions.emplace_back(std::make_pair(score, i));
    }

    if (scoresOfTransitions.empty())
    {
      elements[index].config.printForDebug(stderr);
      util::myThrow("No suitable transition found !");
    }

    std::sort(scoresOfTransitions.rbegin(), scoresOfTransitions.rend());

    while (!scoresOfTransitions.empty() and scoresOfTransitions.back().first < threshold)
      scoresOfTransitions.pop_back();

    if (width > 1)
      for (unsigned int i = 1; i < scoresOfTransitions.size(); i++)
      {
        elements.emplace_back(elements[index], scoresOfTransitions[i].second);
        elements.back().name.push_back(std::to_string(i));
        elements.back().totalProbability += classifier.isRegression() ? 1.0 : scoresOfTransitions[i].first;
        elements.back().config.setChosenActionScore(scoresOfTransitions[i].first);
        elements.back().nbTransitions++;
        elements.back().meanProbability = elements.back().totalProbability / elements.back().nbTransitions;
        elements.back().entropy = entropy;
      }

    elements[index].nextTransition = scoresOfTransitions[0].second;
    elements[index].totalProbability += classifier.isRegression() ? 1.0 : scoresOfTransitions[0].first;
    elements[index].config.setChosenActionScore(scoresOfTransitions[0].first);
    elements[index].nbTransitions++;
    elements[index].name.push_back("0");
    elements[index].meanProbability = 0.0;
    elements[index].meanProbability = elements[index].totalProbability / elements[index].nbTransitions;
    elements[index].entropy = entropy;

    if (debug)
    {
      fmt::print(stderr, "Element {:<3} Probability={} Name={}\n", index, elements[index].meanProbability, util::join(":", elements[index].name));
      elements[index].config.printForDebug(stderr);
      std::vector<std::pair<float,std::string>> toPrint;
      for (unsigned int i = 0; i < prediction.size(0); i++)
      {
        float score = prediction[i].item<float>();
        std::string nicePrint = fmt::format("{} {:7.2f} {}", appliableTransitions[i] ? "*" : " ", score, machine.getTransitionSet(elements[index].config.getState()).getTransition(i)->getName());
        toPrint.emplace_back(std::make_pair(score,nicePrint));
      }
      std::sort(toPrint.rbegin(), toPrint.rend());
      for (unsigned int i = 0; i < 5 and i < toPrint.size(); i++)
        fmt::print(stderr, "{}\n", toPrint[i].second);
    }
  }

  std::sort(elements.begin(), elements.end(), [](const Element & a, const Element & b)
      {
        return a.meanProbability > b.meanProbability; 
      });

  while (elements.size() > width)
    elements.pop_back();

  for (auto & element : elements)
  {
    if (element.ended)
      continue;

    auto & config = element.config;

    auto * transition = machine.getTransitionSet(config.getState()).getTransition(element.nextTransition);

    transition->apply(config, element.entropy);
    config.addToHistory(transition->getName());

    auto movement = config.getStrategy().getMovement(config, transition->getName());
    if (movement == Strategy::endMovement)
    {
      element.ended = true;
      continue;
    }

    config.setState(movement.first);
    config.moveWordIndexRelaxed(movement.second);
  }

  if (debug)
    fmt::print(stderr, "{:*<{}}BEAM END{:*<{}}\n", "", 37, "", 38);
}

bool Beam::isEnded() const
{
  return ended;
}

