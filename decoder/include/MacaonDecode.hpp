#ifndef MACAONDECODE__H
#define MACAONDECODE__H

#include <boost/program_options.hpp>

namespace po = boost::program_options;

class MacaonDecode
{
  private :

  int argc;
  char ** argv;

  private :

  po::options_description getOptionsDescription();
  po::variables_map checkOptions(po::options_description & od);

  public :

  int main();
  MacaonDecode(int argc, char ** argv);

};

#endif
