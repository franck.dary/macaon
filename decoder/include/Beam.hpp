#ifndef BEAM__H
#define BEAM__H

#include <vector>
#include <string>
#include "BaseConfig.hpp"
#include "ReadingMachine.hpp"

class Beam
{
  public :

  class Element
  {
    public :

    BaseConfig config;
    int nextTransition{-1};
    boost::circular_buffer<std::string> name{20};
    float meanProbability{0.0};
    int nbTransitions = 0;
    double totalProbability{0.0};
    bool ended{false};
    float entropy{0.0};

    public :

    Element(const Element & other, int nextTransition);
    Element(const BaseConfig & model);
  };

  private :

  std::size_t width;
  float threshold;
  std::vector<Element> elements;
  bool ended{false};

  public :

  Beam(std::size_t width, float threshold, BaseConfig & model, const ReadingMachine & machine);
  Element & operator[](std::size_t index);
  void update(ReadingMachine & machine, bool debug);
  bool isEnded() const;
};

#endif
