#ifndef DICTHOLDER__H
#define DICTHOLDER__H

#include <memory>
#include <filesystem>
#include "Dict.hpp"
#include "NameHolder.hpp"

class DictHolder : public NameHolder
{
  private :

  static constexpr const char * filenameTemplate = "{}.dict";

  std::unique_ptr<Dict> dict;
  bool pretrained{false};

  private :

  std::string filename() const;

  public :

  DictHolder();
  void saveDict(std::filesystem::path path);
  void loadDict(std::filesystem::path path);
  Dict & getDict();
  bool dictIsPretrained();
  void dictSetPretrained(bool pretrained);
};

#endif

