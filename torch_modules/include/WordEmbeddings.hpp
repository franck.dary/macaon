#ifndef WORDEMBEDDINGS__H
#define WORDEMBEDDINGS__H

#include "torch/torch.h"

class WordEmbeddingsImpl : public torch::nn::Module
{
  private :

  static bool scaleGradByFreq;
  static bool canTrainPretrained;
  static float maxNorm;

  private :
  
  torch::nn::Embedding normalEmbeddings{nullptr};
  torch::nn::Embedding specialEmbeddings{nullptr};

  public :

  static void setScaleGradByFreq(bool scaleGradByFreq);
  static void setMaxNorm(float maxNorm);
  static void setCanTrainPretrained(bool value);
  static bool getCanTrainPretrained();

  WordEmbeddingsImpl(std::size_t vocab, std::size_t dim, std::set<std::size_t> specialIndexes);
  torch::nn::Embedding getNormalEmbeddings();
  torch::Tensor forward(torch::Tensor input);
};
TORCH_MODULE(WordEmbeddings);

#endif
