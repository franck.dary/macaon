#ifndef APPLIABLETRANSRANSMODULE__H
#define APPLIABLETRANSRANSMODULE__H

#include <torch/torch.h>
#include "Submodule.hpp"
#include "MyModule.hpp"
#include "LSTM.hpp"
#include "GRU.hpp"

class AppliableTransModuleImpl : public Submodule
{
  private :

  int nbTrans;

  public :

  AppliableTransModuleImpl(std::string name, int nbTrans);
  torch::Tensor forward(torch::Tensor input);
  std::size_t getOutputSize() override;
  std::size_t getInputSize() override;
  void addToContext(torch::Tensor & context, const Config & config) override;
  void registerEmbeddings(bool loadPretrained) override;
};
TORCH_MODULE(AppliableTransModule);

#endif

