#ifndef STATEHOLDER__H
#define STATEHOLDER__H

#include <string>

class StateHolder
{
  private :

  std::string state;

  public :

  const std::string & getState() const;
  void setState(const std::string & state);
};

#endif

