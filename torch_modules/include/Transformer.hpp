#ifndef TRANSFORMER__H
#define TRANSFORMER__H

#include <torch/torch.h>
#include "MyModule.hpp"

class TransformerImpl : public MyModule
{
  private :

  int inputSize;
  torch::nn::TransformerEncoder encoder{nullptr};
  torch::nn::TransformerEncoderLayer layer{nullptr};
  torch::Tensor pe;

  public :

  TransformerImpl(int inputSize, int hiddenSize, ModuleOptions options);
  torch::Tensor forward(torch::Tensor input);
  int getOutputSize(int sequenceLength);
};
TORCH_MODULE(Transformer);

#endif

