#ifndef Concat__H
#define Concat__H

#include <torch/torch.h>
#include "MyModule.hpp"

class ConcatImpl : public MyModule
{
  private :

  int inputSize;
  int outputSize;
  torch::nn::Linear dimReduce{nullptr};

  public :

  ConcatImpl(int inputSize, int outputSize);
  torch::Tensor forward(torch::Tensor input);
  int getOutputSize(int sequenceLength);
};
TORCH_MODULE(Concat);

#endif

