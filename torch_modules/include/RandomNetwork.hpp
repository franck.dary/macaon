#ifndef RANDOMNETWORK__H
#define RANDOMNETWORK__H

#include "NeuralNetwork.hpp"

class RandomNetworkImpl : public NeuralNetworkImpl
{
  private :

  std::map<std::string,std::size_t> nbOutputsPerState;

  public :

  RandomNetworkImpl(std::string name, std::map<std::string,std::size_t> nbOutputsPerState);
  torch::Tensor forward(torch::Tensor input, const std::string & state) override;
  torch::Tensor extractContext(Config &) override;
  void registerEmbeddings(bool loadPretrained) override;
  void saveDicts(std::filesystem::path path) override;
  void loadDicts(std::filesystem::path path) override;
  void setDictsState(Dict::State state) override;
  void setCountOcc(bool countOcc) override;
  void removeRareDictElements(float rarityThreshold) override;
};

#endif
