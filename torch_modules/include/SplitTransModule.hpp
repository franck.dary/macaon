#ifndef SPLITTRANSMODULE__H
#define SPLITTRANSMODULE__H

#include <torch/torch.h>
#include "Submodule.hpp"
#include "MyModule.hpp"
#include "LSTM.hpp"
#include "GRU.hpp"
#include "Concat.hpp"
#include "WordEmbeddings.hpp"

class SplitTransModuleImpl : public Submodule
{
  private :

  WordEmbeddings wordEmbeddings{nullptr};
  std::shared_ptr<MyModule> myModule{nullptr};
  int maxNbTrans;
  int inSize;

  public :

  SplitTransModuleImpl(std::string name, int maxNbTrans, const std::string & definition);
  torch::Tensor forward(torch::Tensor input);
  std::size_t getOutputSize() override;
  std::size_t getInputSize() override;
  void addToContext(torch::Tensor & context, const Config & config) override;
  void registerEmbeddings(bool loadPretrained) override;
};
TORCH_MODULE(SplitTransModule);

#endif

