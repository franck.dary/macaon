#ifndef CUSTOMHINGELOSS__H
#define CUSTOMHINGELOSS__H

#include <torch/torch.h>

class CustomHingeLoss
{
  public :

  torch::Tensor operator()(torch::Tensor prediction, torch::Tensor gold);
};

#endif
