#ifndef SUBMODULE__H
#define SUBMODULE__H

#include <torch/torch.h>
#include <filesystem>
#include "Config.hpp"
#include "DictHolder.hpp"

class Submodule : public torch::nn::Module, public DictHolder
{
  private :

  static bool reloadPretrained;

  protected :

  std::size_t firstInputIndex{0};

  public :

  static void setReloadPretrained(bool reloadPretrained);

  void setFirstInputIndex(std::size_t firstInputIndex);
  void loadPretrainedW2vEmbeddings(torch::nn::Embedding embeddings, std::filesystem::path path, std::string prefix, bool loadPretrained);
  virtual std::size_t getOutputSize() = 0;
  virtual std::size_t getInputSize() = 0;
  virtual void addToContext(torch::Tensor & context, const Config & config) = 0;
  virtual torch::Tensor forward(torch::Tensor input) = 0;
  virtual void registerEmbeddings(bool loadPretrained) = 0;
  std::function<std::string(const std::string &)> getFunction(const std::string functionNames);
};

#endif

