#ifndef LSTM__H
#define LSTM__H

#include <torch/torch.h>
#include "MyModule.hpp"

class LSTMImpl : public MyModule
{
  private :

  torch::nn::LSTM lstm{nullptr};
  bool outputAll;

  public :

  LSTMImpl(int inputSize, int outputSize, ModuleOptions options);
  torch::Tensor forward(torch::Tensor input);
  int getOutputSize(int sequenceLength);
};
TORCH_MODULE(LSTM);

#endif

