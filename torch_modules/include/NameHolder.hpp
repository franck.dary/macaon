#ifndef NAMEHOLDER__H
#define NAMEHOLDER__H

#include <string>

class NameHolder
{
  private :

  std::string name;

  public :

  const std::string & getName() const;
  void setName(const std::string & name);
};

#endif

