#include "Transformer.hpp"
#include "fmt/core.h"

TransformerImpl::TransformerImpl(int inputSize, int hiddenSize, ModuleOptions options) : inputSize(inputSize)
{
  int numHeads = std::get<1>(options);
  int numLayers = std::get<2>(options);
  float dropout = std::get<3>(options);
  auto layerOptions = torch::nn::TransformerEncoderLayerOptions(inputSize, numHeads)
                        .dim_feedforward(hiddenSize)
                        .dropout(dropout);

  layer = register_module("layer", torch::nn::TransformerEncoderLayer(layerOptions));

  auto encoderOptions = torch::nn::TransformerEncoderOptions(layer, numLayers);

  encoder = register_module("encoder", torch::nn::TransformerEncoder(encoderOptions));

  // Positional embeddings
  static constexpr int maxLen = 5000;
  pe = torch::zeros({maxLen, inputSize});
  auto position = torch::arange(0, maxLen, torch::kFloat).unsqueeze(1);
  auto divTerm = torch::exp(torch::arange(0,inputSize,2, torch::kFloat) * (-log(10000.0) / inputSize));

  auto sins = torch::sin(position * divTerm);
  auto coss = torch::cos(position * divTerm);
  for (unsigned int i = 0; i < pe.size(0); i++)
    pe[i] = torch::cat({sins[i], coss[i]});

  pe = pe.unsqueeze(0).transpose(0,1);
  register_buffer("pe", pe);
}

torch::Tensor TransformerImpl::forward(torch::Tensor input)
{
  return torch::transpose(encoder(torch::transpose(input, 0, 1)+torch::narrow(pe, 0, 0, input.size(1))), 0, 1);
}

int TransformerImpl::getOutputSize(int sequenceLength)
{
  return inputSize * sequenceLength;
}

