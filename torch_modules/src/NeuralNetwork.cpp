#include "NeuralNetwork.hpp"

torch::Device NeuralNetworkImpl::device(getPreferredDevice());

float NeuralNetworkImpl::entropy(torch::Tensor probabilities)
{
  if (probabilities.dim() != 1)
    util::myThrow("Invalid probabilities tensor");

  float entropy = 0.0;
  for (unsigned int i = 0; i < probabilities.size(0); i++)
  {
    if (probabilities[i].item<float>() > 0.01)
      entropy -= (probabilities[i] * torch::log(probabilities[i])).item<float>();
  }

  if (entropy < 0.01)
    entropy = 0.0;

  return entropy;
}

torch::Device NeuralNetworkImpl::getPreferredDevice()
{
  return torch::cuda::is_available() ? torch::kCUDA : torch::kCPU;
}

torch::Device NeuralNetworkImpl::getDevice()
{
  return device;
}

void NeuralNetworkImpl::setDevice(torch::Device device)
{
  NeuralNetworkImpl::device = device;
}

