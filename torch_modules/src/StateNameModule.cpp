#include "StateNameModule.hpp"

StateNameModuleImpl::StateNameModuleImpl(std::string name, const std::string & definition)
{
  setName(name);
  std::regex regex("(?:(?:\\s|\\t)*)Out\\{(.*)\\}(?:(?:\\s|\\t)*)");
  if (!util::doIfNameMatch(regex, definition, [this,&definition](auto sm)
        {
          try
          {
            outSize = std::stoi(sm.str(1));
          } catch (std::exception & e) {util::myThrow(fmt::format("{} in '{}'",e.what(),definition));}
        }))
    util::myThrow(fmt::format("invalid definition '{}'", definition));
}

torch::Tensor StateNameModuleImpl::forward(torch::Tensor input)
{
  return embeddings(input.narrow(1,firstInputIndex,1).squeeze(1));
}

std::size_t StateNameModuleImpl::getOutputSize()
{
  return outSize;
}

std::size_t StateNameModuleImpl::getInputSize()
{
  return 1;
}

void StateNameModuleImpl::addToContext(torch::Tensor & context, const Config & config)
{
  auto & dict = getDict();
  context[firstInputIndex] = dict.getIndexOrInsert(config.getState(), "");
}

void StateNameModuleImpl::registerEmbeddings(bool)
{
  if (!embeddings)
    embeddings = register_module("embeddings", WordEmbeddings(getDict().size(), outSize, std::set<std::size_t>()));
}

