#include "RandomNetwork.hpp"

RandomNetworkImpl::RandomNetworkImpl(std::string name, std::map<std::string,std::size_t> nbOutputsPerState) : nbOutputsPerState(nbOutputsPerState)
{
  setName(name);
}

torch::Tensor RandomNetworkImpl::forward(torch::Tensor input, const std::string & state)
{
  if (input.dim() == 1)
    input = input.unsqueeze(0);

  return torch::randn({input.size(0), (long)nbOutputsPerState[state]}, torch::TensorOptions().device(NeuralNetworkImpl::getDevice()).requires_grad(true));
}

torch::Tensor RandomNetworkImpl::extractContext(Config &)
{
  torch::Tensor context;
  return context;
}

void RandomNetworkImpl::registerEmbeddings(bool)
{
}

void RandomNetworkImpl::saveDicts(std::filesystem::path)
{
}

void RandomNetworkImpl::loadDicts(std::filesystem::path)
{
}

void RandomNetworkImpl::setDictsState(Dict::State)
{
}

void RandomNetworkImpl::setCountOcc(bool)
{
}

void RandomNetworkImpl::removeRareDictElements(float)
{
}

