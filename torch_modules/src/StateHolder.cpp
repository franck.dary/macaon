#include "StateHolder.hpp"
#include "util.hpp"

const std::string & StateHolder::getState() const
{
  if (state.empty())
    util::myThrow("state is empty");

  return state;
}

void StateHolder::setState(const std::string & state)
{
  this->state = state;
}

