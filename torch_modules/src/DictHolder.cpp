#include "DictHolder.hpp"
#include "fmt/core.h"

DictHolder::DictHolder()
{
  dict.reset(new Dict(Dict::State::Open));
}

std::string DictHolder::filename() const
{
  return fmt::format(filenameTemplate, getName());
}

void DictHolder::saveDict(std::filesystem::path path)
{
  dict->save(path / filename(), Dict::Encoding::Ascii);
}

void DictHolder::loadDict(std::filesystem::path path)
{
  auto dictPath = path / filename();
  if (std::filesystem::exists(dictPath))
    dict.reset(new Dict(dictPath.c_str(), dict->getState()));
}

Dict & DictHolder::getDict()
{
  return *dict;
}

bool DictHolder::dictIsPretrained()
{
  return pretrained;
}

void DictHolder::dictSetPretrained(bool pretrained)
{
  this->pretrained = pretrained;
}

