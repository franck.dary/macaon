#include "NumericColumnModule.hpp"
#include "NeuralNetwork.hpp"

NumericColumnModuleImpl::NumericColumnModuleImpl(std::string name, const std::string & definition)
{
  setName(name);
  std::regex regex("(?:(?:\\s|\\t)*)Column\\{(.*)\\}(?:(?:\\s|\\t)*)Buffer\\{(.*)\\}(?:(?:\\s|\\t)*)Stack\\{(.*)\\}(?:(?:\\s|\\t)*)(\\S+)\\{(.*)\\}(?:(?:\\s|\\t)*)Out\\{(.*)\\}(?:(?:\\s|\\t)*)DefaultValue\\{(.*)\\}(?:(?:\\s|\\t)*)");
  if (!util::doIfNameMatch(regex, definition, [this,&definition](auto sm)
        {
          try
          {
            column = sm.str(1);

            for (auto & index : util::split(sm.str(2), ' '))
              focusedBuffer.emplace_back(std::stoi(index));

            for (auto & index : util::split(sm.str(3), ' '))
              focusedStack.emplace_back(std::stoi(index));

            auto subModuleType = sm.str(4);
            auto subModuleArguments = util::split(sm.str(5), ' ');

            auto options = MyModule::ModuleOptions(true)
              .bidirectional(std::stoi(subModuleArguments[0]))
              .num_layers(std::stoi(subModuleArguments[1]))
              .dropout(std::stof(subModuleArguments[2]))
              .complete(std::stoi(subModuleArguments[3]));

            int outSize = std::stoi(sm.str(6));

            defaultValue = std::stoi(sm.str(7));

            if (subModuleType == "LSTM")
              myModule = register_module("myModule", LSTM(1, outSize, options));
            else if (subModuleType == "GRU")
              myModule = register_module("myModule", GRU(1, outSize, options));
            else if (subModuleType == "Concat")
              myModule = register_module("myModule", Concat(0,0));
            else
              util::myThrow(fmt::format("unknown sumodule type '{}'", subModuleType));
          } catch (std::exception & e) {util::myThrow(fmt::format("{} in '{}'",e.what(),definition));}
        }))
    util::myThrow(fmt::format("invalid definition '{}'", definition));
}

torch::Tensor NumericColumnModuleImpl::forward(torch::Tensor input)
{
  auto context = input.narrow(1, firstInputIndex, getInputSize());
  auto values = context.to(torch::kFloat).unsqueeze(-1) / util::float2longScale;
  return myModule->forward(values).reshape({input.size(0), -1});
}

std::size_t NumericColumnModuleImpl::getOutputSize()
{
  return myModule->getOutputSize(getInputSize());
}

std::size_t NumericColumnModuleImpl::getInputSize()
{
  return focusedBuffer.size() + focusedStack.size();
}

void NumericColumnModuleImpl::addToContext(torch::Tensor & context, const Config & config)
{
  std::vector<long> focusedIndexes;

  for (int index : focusedBuffer)
    focusedIndexes.emplace_back(config.getRelativeWordIndex(index));

  for (int index : focusedStack)
    if (config.hasStack(index))
      focusedIndexes.emplace_back(config.getStack(index));
    else
      focusedIndexes.emplace_back(-1);

  int insertIndex = 0;
  for (auto index : focusedIndexes)
  {
    double res = 0.0;
    if (index >= 0)
    {
      auto value = std::string(config.getAsFeature(column, index));
      try {res = (value == "_" or value == "NA") ? defaultValue : std::stof(value);}
      catch (std::exception & e)
        {util::myThrow(fmt::format("{} for '{}'", e.what(), value));}
    }

    context[firstInputIndex+insertIndex] = util::float2long(res);
    insertIndex++;
  }
}

void NumericColumnModuleImpl::registerEmbeddings(bool)
{
}

