#include "NameHolder.hpp"
#include "util.hpp"

const std::string & NameHolder::getName() const
{
  if (name.empty())
    util::myThrow("name is empty");

  return name;
}

void NameHolder::setName(const std::string & name)
{
  this->name = name;
}

