#include "HistoryModule.hpp"

HistoryModuleImpl::HistoryModuleImpl(std::string name, const std::string & definition)
{
  setName(name);
  std::regex regex("(?:(?:\\s|\\t)*)NbElem\\{(.*)\\}(?:(?:\\s|\\t)*)(\\S+)\\{(.*)\\}(?:(?:\\s|\\t)*)In\\{(.*)\\}(?:(?:\\s|\\t)*)Out\\{(.*)\\}(?:(?:\\s|\\t)*)");
  if (!util::doIfNameMatch(regex, definition, [this,&definition](auto sm)
        {
          try
          {
            maxNbElements = std::stoi(sm.str(1));

            auto subModuleType = sm.str(2);
            auto subModuleArguments = util::split(sm.str(3), ' ');

            auto options = MyModule::ModuleOptions(true)
              .bidirectional(std::stoi(subModuleArguments[0]))
              .num_layers(std::stoi(subModuleArguments[1]))
              .dropout(std::stof(subModuleArguments[2]))
              .complete(std::stoi(subModuleArguments[3]));

            inSize = std::stoi(sm.str(4));
            int outSize = std::stoi(sm.str(5));

            if (subModuleType == "LSTM")
              myModule = register_module("myModule", LSTM(inSize, outSize, options));
            else if (subModuleType == "GRU")
              myModule = register_module("myModule", GRU(inSize, outSize, options));
            else if (subModuleType == "CNN")
              myModule = register_module("myModule", CNN(inSize, outSize, options));
            else if (subModuleType == "Concat")
              myModule = register_module("myModule", Concat(inSize, outSize));
            else
              util::myThrow(fmt::format("unknown sumodule type '{}'", subModuleType));

          } catch (std::exception & e) {util::myThrow(fmt::format("{} in '{}'",e.what(),definition));}
        }))
    util::myThrow(fmt::format("invalid definition '{}'", definition));
}

torch::Tensor HistoryModuleImpl::forward(torch::Tensor input)
{
  return myModule->forward(wordEmbeddings(input.narrow(1, firstInputIndex, maxNbElements))).reshape({input.size(0), -1});
}

std::size_t HistoryModuleImpl::getOutputSize()
{
  return myModule->getOutputSize(maxNbElements);
}

std::size_t HistoryModuleImpl::getInputSize()
{
  return maxNbElements;
}

void HistoryModuleImpl::addToContext(torch::Tensor & context, const Config & config)
{
  auto & dict = getDict();

  std::string prefix = "HISTORY";

  for (int i = 0; i < maxNbElements; i++)
    if (config.hasHistory(i))
      context[firstInputIndex+i] = dict.getIndexOrInsert(config.getHistory(i), prefix);
    else
      context[firstInputIndex+i] = dict.getIndexOrInsert(Dict::nullValueStr, prefix);
}

void HistoryModuleImpl::registerEmbeddings(bool)
{
  if (!wordEmbeddings)
    wordEmbeddings = register_module("embeddings", WordEmbeddings(getDict().size(), inSize, std::set<std::size_t>()));
}

