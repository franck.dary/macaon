#include "Concat.hpp"

ConcatImpl::ConcatImpl(int inputSize, int outputSize) : inputSize(inputSize), outputSize(outputSize)
{
  if (inputSize and outputSize) // if one of these is null, don't use a linear layer
    dimReduce = register_module("dimReduce", torch::nn::Linear(inputSize, outputSize));
}

torch::Tensor ConcatImpl::forward(torch::Tensor input)
{
  if (dimReduce)
    return dimReduce(input).view({input.size(0), -1});
  return input.view({input.size(0), -1});
}

int ConcatImpl::getOutputSize(int sequenceLength)
{
  if (outputSize)
    return sequenceLength * outputSize;
  return sequenceLength;
}

