#include "CNN.hpp"
#include "fmt/core.h"

CNNImpl::CNNImpl(int inputSize, int outputSize, ModuleOptions) : outputSize(outputSize)
{
  for (auto & windowSize : windowSizes)
  {
    std::string moduleName = fmt::format("cnn_window_{}", windowSize);
    auto kernel = torch::ExpandingArray<2>({windowSize, inputSize});
    auto opts = torch::nn::Conv2dOptions(1, outputSize, kernel).padding({windowSize-1, 0});
    CNNs.emplace_back(register_module(moduleName, torch::nn::Conv2d(opts)));
  }
}

torch::Tensor CNNImpl::forward(torch::Tensor input)
{
  std::vector<torch::Tensor> windows;
  input = input.unsqueeze(1);

  for (unsigned int i = 0; i < CNNs.size(); i++)
  {
    auto convOut = CNNs[i](input).squeeze(-1);
    auto pooled = torch::max_pool1d(convOut, convOut.size(-1));
    windows.emplace_back(pooled);
  }

  return torch::cat(windows, -1).view({input.size(0), -1});
}

int CNNImpl::getOutputSize(int)
{
  return outputSize*windowSizes.size();
}

