# Transition Set

Transition Set files (.ts) are the link between the [Classifier](classifier.md) and the [Strategy](strategy.md).\
Each state is linked to a Transition Set file (see [Classifier](classifier.md)), so that when the classifier is making a prediction,\
the index of the most activated output neuron corresponds to an index of the Transition Set file and thus to a Transition.\
When the classifier has predicted the next Transition, the [Strategy](strategy.md) is used to determine the next state and the relative movement of the word index.

Each line of a .ts file will be of the form `[<stateName>] transitionName [arguments]` where [elements] are optional.\
stateName restricts the transition to a certain state.

## Transitions

Here is the list of all available transitions along with their possible arguments :
* `WRITE $1.$2 $3 $4`\
	Write a string to a cell. E.g. `WRITE b.0 UPOS VERB`
	* $1 : `b` or `s`, writing to the buffer or the stack.
	* $2 : Relative index to write into.
	* $3 : The name of the tape to write into.
	* $4 : The string to write.
* `ADD $1.$2 $3 $4`\
	Add a string to a cell. E.g. `ADD b.0 FEATS Gender=Fem`
	* $1 : `b` or `s`, writing to the buffer or the stack.
	* $2 : Relative index to write into.
	* $3 : The name of the tape to write into.
	* $4 : The string to append.
* `eager_SHIFT`\
	Shift transition in arc_eager transition based parsing. Push the current word index onto the stack.
* `standard_SHIFT`\
	Shift transition in arc_standard transition based parsing. Push the current word index onto the stack.

TODO
