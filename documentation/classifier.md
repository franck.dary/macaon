# Classifier

The classifier is a neural network, at each step it takes the current [configuration](readingMachine.md) as input and predicts the next [transition](transitionSet.md) to take.

The classifier must be defined in the [Reading Machine](readingMachine.md) file.\
It's definition is made of three parts :
* In the first part we have to define :
	* A name.
	* For each state, what is the [Transition Set](transitionSet.md) file associated to it.
	* For each state, we can specify a scalar for the training loss function to be multiplied by. Default value is 1.0.
	* The network type : Random (no neural network) or Modular (see below).

	Example :
	```
	Classifier : tokeparser
	{
	  Transitions : {tokenizer,data/tokenizer.ts tagger,data/tagger.ts morpho,data/morpho_whole.ts lemmatizer_rules,data/lemmatizer_rules.ts lemmatizer_case,data/lemmatizer_case.ts parser,data/parser_eager_rel_strict.ts segmenter,data/segmenter.ts}
	  LossMultiplier : {}
		Network type : Modular
	  Contextual : Window{-10 10} Columns{FORM} LSTM{1 1 0 1} In{64} Out{128} w2v{FORM,data/FORM.w2v} Targets{b.-3 b.-2 b.-1 b.0 b.1 b.2 s.0 s.1 s.2 b.0.0 s.0.0 s.0.-1 s.1.0 s.1.-1 s.2.0 s.2.-1}
	  Context : Targets{b.-3 b.-2 b.-1 b.0 b.1 b.2 s.0 s.1 s.2 b.0.0 s.0.0 s.0.-1 s.1.0 s.1.-1 s.2.0 s.2.-1} Columns{EOS ID UPOS FEATS DEPREL} LSTM{1 1 0 1} In{64} Out{64} w2v{}
	  Focused : Column{prefix3:FORM} NbElem{3} Buffer{0} Stack{} LSTM{1 1 0 1} In{64} Out{64} w2v{}
	  Focused : Column{suffix3:FORM} NbElem{3} Buffer{0} Stack{} LSTM{1 1 0 1} In{64} Out{64} w2v{}
		RawInput : Left{5} Right{10} LSTM{1 1 0.0 1} In{32} Out{32}
	  History : NbElem{10} LSTM{1 1 0 1} In{32} Out{32}
		SplitTrans : LSTM{1 1 0.0 1} In{64} Out{64}
		InputDropout : 0.3
		MLP : {1600 0.3 1600 0.3}
		End
	  Optimizer : Adagrad {0.01 0.000001 0 0.0000000001}
	  Type : classification
	  Loss : crossentropy
	}
	```

* In the third part, we must define the hyperparameters of the optimizer algorithm. Currently available optimizers are : 
	* Adam {learningRate beta1 beta2 epsilon weightDecay useAMSGRAD}

	Example :

	```
	  Optimizer : Adam {0.0002 0.9 0.999 0.00000001 0.00001 true}
	}
	```

## Network type : Random

The predictions will be chosen at random. There is no parameters to be learned.\
The purpose of this network type is to debug a [Reading Machine](readingMachine.md) topology, because it is very fast.\
There is nothing to define, you can put the 'End' line just after the line 'Network type : Random'.


## Network type : Modular

Each line of the definition of the Modular network type correspond to a module.\
The order of the modules in the definition are not important, you can also use a module multiple times.\
There are two mandatory modules : 
* `MLP : {Layer1Size Layer1Dropout...}`\
	Definition of the Multi Layer Perceptron, that will take as input the concatenation of the outputs of all other modules, and will act as the output layers of the neural network.\
	The last layer (output layer) is not part of the definition because its size is dynamically deduced from the number of outgoing transitions of the current state. Thus you only need to define the hidden layers of the MLP.\
	Example to define a MLP with 2 hidden layers of respective sizes 2048 and 1024 and of respective dropouts 0.3 and 0.1 :
	```
	MLP : {2048 0.3 1024 0.1}
	```
* `InputDropout : scalar`\
	Dropout (between 0.0 and 1.0) to apply to the input of the MLP.
	E.g. `InputDropout : 0.5`

And then there is a list of optional modules you can choose from :
* `StateName : Out{embeddingSize}`\
	An embedding of size *embeddingSize* representing the name of the current state.
* `Context : Buffer{$1} Stack{$2} Columns{$3} $4{$5 $6 $7 $8} In{$9} Out{$10}`\
	An embedding capturing a relative context around the machine's current word index.
	* $1 : List of relative buffer indexes to capture. E.g. `{-3 -2 -1 0 1 2}`.
	* $2 : List of stack indexes to capture. E.g. `{2 1 0}`.
	* $3 : List of column names to capture. E.g. `{FORM UPOS}`.
	* $4 : Type of recurrent module to use to generate the context embedding, LSTM or GRU.
	* $5 : Use bidirectional RNN ? 1 or 0.
	* $6 : Number of RNN layers to use (minimum 1).
	* $7 : Dropout to use after RNN hidden layers. Must be 0 if number of layers is 1.
	* $8 : 1 to concatenate all of the RNN hidden states, 0 to only use the last RNN hidden state.
	* $9 : Size of the embeddings used to feed the RNN.
	* $10 : Size of the hidden states of the RNN.
* `Focused Column{$1} NbElem{$2} Buffer{$3} Stack{$4} $5{$6 $7 $8 $9} In{$10} Out{$11}`\
	An embedding capturing a specific string, viewed as a sequence of elements.\
	If Column = FORM elements are the letters, if Column = FEATS elements are the traits.
	* $1 : Column name to capture. E.g. `{FORM}`.
	* $2 : Maximum number of elements (example max number of letters in a word).
	* $3 : List of relative buffer indexes to capture. E.g. `{-3 -2 -1 0 1 2}`.
	* $4 : List of stack indexes to capture. E.g. `{2 1 0}`.
	* $5 : Type of recurrent module to use to generate the context embedding, LSTM or GRU.
	* $6 : Use bidirectional RNN ? 1 or 0.
	* $7 : Number of RNN layers to use (minimum 1).
	* $8 : Dropout to use after RNN hidden layers. Must be 0 if number of layers is 1.
	* $9 : 1 to concatenate all of the RNN hidden states, 0 to only use the last RNN hidden state.
	* $10 : Size of the embeddings used to feed the RNN.
	* $11 : Size of the hidden states of the RNN.
* `DepthLayerThree : Columns{$1} Buffer{$2} Stack{$3} LayerSizes{$4} $5{$6 $7 $8 $9} In{$10} Out{$11}`\
	For each captured index, compute an embedding of the syntactic tree rooted by this index.
	* $1 : List of column names to capture. E.g. `{DEPREL UPOS}`.
	* $2 : List of relative buffer indexes to capture. E.g. `{-3 -2 -1}`.
	* $3 : List of stack indexes to capture. E.g. `{2 1}`.
	* $4 : List of sizes, the length of the list is the maximum depth of the childs, and each size is the maximum number of child for each depth. E.g. `{3 6}`.
	* $5 : Type of recurrent module to use to generate the context embedding, LSTM or GRU.
	* $6 : Use bidirectional RNN ? 1 or 0.
	* $7 : Number of RNN layers to use (minimum 1).
	* $8 : Dropout to use after RNN hidden layers. Must be 0 if number of layers is 1.
	* $9 : 1 to concatenate all of the RNN hidden states, 0 to only use the last RNN hidden state.
	* $10 : Size of the embeddings used to feed the RNN.
	* $11 : Size of the hidden states of the RNN.
* `History : NbElem{$1} $2{$3 $4 $5 $6} In{$7} Out{$8}`\
	An embedding representing the history of the previous transitions.
	* $1 : Take into account only the $1 last transitions.
	* $2 : Type of recurrent module to use to generate the context embedding, LSTM or GRU.
	* $3 : Use bidirectional RNN ? 1 or 0.
	* $4 : Number of RNN layers to use (minimum 1).
	* $5 : Dropout to use after RNN hidden layers. Must be 0 if number of layers is 1.
	* $6 : 1 to concatenate all of the RNN hidden states, 0 to only use the last RNN hidden state.
	* $7 : Size of the embeddings used to feed the RNN.
	* $8 : Size of the hidden states of the RNN.

* `RawInput : Left{$1} Right{$2} $3{$4 $5 $6 $7} In{$8} Out{$9}`\
	Embedding representing a window of the raw text input (centered around the character index).
	* $1 : Size of the left window (how many character to the left of the center do the machine sees). E.g. 5.
	* $2 : Size of the right window (how many character to the right of the center do the machine sees). E.g. 5.
	* $3 : Type of recurrent module to use to generate the context embedding, LSTM or GRU.
	* $4 : Use bidirectional RNN ? 1 or 0.
	* $5 : Number of RNN layers to use (minimum 1).
	* $6 : Dropout to use after RNN hidden layers. Must be 0 if number of layers is 1.
	* $7 : 1 to concatenate all of the RNN hidden states, 0 to only use the last RNN hidden state.
	* $8 : Size of the embeddings used to feed the RNN.
	* $9 : Size of the hidden states of the RNN.
* `SplitTrans : $1{$2 $3 $4 $5} In{$6} Out{$7}`\
	An embedding representing the currently appliable split transitions (see [Transition Set](transitionSet.md)).
	* $1 : Type of recurrent module to use to generate the context embedding, LSTM or GRU.
	* $2 : Use bidirectional RNN ? 1 or 0.
	* $3 : Number of RNN layers to use (minimum 1).
	* $4 : Dropout to use after RNN hidden layers. Must be 0 if number of layers is 1.
	* $5 : 1 to concatenate all of the RNN hidden states, 0 to only use the last RNN hidden state.
	* $6 : Size of the embeddings used to feed the RNN.
	* $7 : Size of the hidden states of the RNN.
