#ifndef BASECONFIG__H
#define BASECONFIG__H

#include <string>
#include <vector>
#include <unordered_map>
#include <boost/flyweight.hpp>
#include "util.hpp"
#include "Config.hpp"

class SubConfig;

class BaseConfig : public Config
{
  private :

  std::vector<std::string> colIndex2Name;
  std::unordered_map<std::string, int> colName2Index;

  private :

  void createColumns(std::string mcd);
  void readRawInput(std::string_view rawFilename);
  void readTSVInput(const std::vector<std::vector<std::string>> & sentences, const std::vector<int> & sentencesIndexes);

  public :

  BaseConfig(std::string mcd, const std::vector<std::vector<std::string>> & sentences, const util::utf8string & rawFilename, const std::vector<int> & sentencesIndexes);
  BaseConfig(const BaseConfig & other);
  BaseConfig & operator=(const BaseConfig & other) = default;

  std::size_t getNbColumns() const override;
  std::size_t getFirstLineIndex() const override;
  std::size_t getColIndex(const std::string & colName) const override;
  bool hasColIndex(const std::string & colName) const override;
  const std::string & getColName(int colIndex) const override;

  friend SubConfig;
};

#endif
