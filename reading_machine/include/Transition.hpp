#ifndef TRANSITION__H
#define TRANSITION__H

#include <vector>
#include "Action.hpp"
#include "Config.hpp"

class Transition
{
  private :

  std::string name;
  std::string state;
  std::vector<Action> sequence;
  std::function<int(const Config & config, const std::map<std::string, int> & links)> costDynamic;
  std::function<int(const Config & config, const std::map<std::string, int> & links)> costStatic;
  std::function<bool(const Config & config)> precondition{[](const Config&){return true;}};

  private :

  static int getNbLinkedWith(int firstIndex, int lastIndex, Config::Object object, int withIndex, const Config & config);
  static int getNbLinkedWithHead(int firstIndex, int lastIndex, Config::Object object, int withIndex, const Config & config);
  static int getNbLinkedWithDeps(int firstIndex, int lastIndex, Config::Object object, int withIndex, const Config & config);

  static int getFirstIndexOfSentence(int baseIndex, const Config & config);
  static int getLastIndexOfSentence(int baseIndex, const Config & config);

  void initWrite(std::string colName, std::string object, std::string index, std::string value);
  void initAdd(std::string colName, std::string object, std::string index, std::string value);
  void initEagerShift();
  void initGoldEagerShift();
  void initStandardShift();
  void initEagerLeft_rel(std::string label);
  void initGoldEagerLeft_rel(std::string label);
  void initEagerRight_rel(std::string label);
  void initGoldEagerRight_rel(std::string label);
  void initStandardLeft_rel(std::string label);
  void initStandardRight_rel(std::string label);
  void initDeprel(std::string label);
  void initEagerLeft();
  void initEagerRight();
  void initReduce_strict();
  void initGoldReduce_strict();
  void initReduce_relaxed();
  void initEOS(int bufferIndex);
  void initNotEOS(int bufferIndex);
  void initNothing();
  void initIgnoreChar();
  void initEndWord();
  void initAddCharToWord(int n);
  void initSplitWord(std::vector<std::string> words);
  void initSplit(int index);
  void initTransformSuffix(std::string fromCol, std::string fromObj, std::string fromIndex, std::string toCol, std::string toObj, std::string toIndex, std::string rule);
  void initUppercase(std::string col, std::string obj, std::string index);
  void initUppercaseIndex(std::string col, std::string obj, std::string index, std::string inIndex);
  void initNothing(std::string col, std::string obj, std::string index);
  void initLowercase(std::string col, std::string obj, std::string index);
  void initLowercaseIndex(std::string col, std::string obj, std::string index, std::string inIndex);
  void initWriteScore(std::string colName, std::string object, std::string index);

  public :

  Transition(const std::string & name);
  void apply(Config & config, float entropy);
  void apply(Config & config);
  bool appliable(const Config & config) const;
  int getCostDynamic(const Config & config, const std::map<std::string, int> & links) const;
  int getCostStatic(const Config & config, const std::map<std::string, int> & links) const;
  const std::string & getName() const;
};

#endif
