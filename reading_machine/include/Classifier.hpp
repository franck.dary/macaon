#ifndef CLASSIFIER__H
#define CLASSIFIER__H

#include <string>
#include <filesystem>
#include "TransitionSet.hpp"
#include "NeuralNetwork.hpp"
#include "LossFunction.hpp"

class Classifier
{
  private :

  std::vector<std::string> knownOptimizers{
    "Adam {lr beta1 beta2 eps decay amsgrad}",
    "Adagrad {lr lr_decay weight_decay eps}",
  };

  std::string name;
  std::map<std::string, std::unique_ptr<TransitionSet>> transitionSets;
  std::map<std::string, float> lossMultipliers;
  std::shared_ptr<NeuralNetworkImpl> nn;
  std::unique_ptr<torch::optim::Optimizer> optimizer;
  std::string optimizerType, optimizerParameters;
  std::vector<std::string> states;
  std::filesystem::path path;
  bool regression{false};
  std::vector<std::tuple<std::string, std::string, std::string>> bannedExamples;
  LossFunction lossFct;

  private :

  void initNeuralNetwork(const std::vector<std::string> & definition, std::size_t curIndex);
  void initModular(const std::vector<std::string> & definition, std::size_t & curIndex, const std::map<std::string,std::size_t> & nbOutputsPerState);
  std::string getLastFilename() const;
  std::string getBestFilename() const;

  public :

  Classifier(const std::string & name, std::filesystem::path path, std::vector<std::string> definition, bool train, bool loadPretrained=false);
  TransitionSet & getTransitionSet(const std::string & state);
  NeuralNetwork & getNN();
  const std::string & getName() const;
  int getNbParameters() const;
  void resetOptimizer();
  void loadOptimizer();
  void saveOptimizer();
  torch::optim::Optimizer & getOptimizer();
  float getLossMultiplier(const std::string & state);
  const std::vector<std::string> & getStates() const;
  void saveDicts();
  void saveBest();
  void saveLast();
  bool isRegression() const;
  LossFunction & getLossFunction();
  bool exampleIsBanned(const Config & config);
  void to(torch::Device device);
};

#endif
