#ifndef STRATEGY__H
#define STRATEGY__H

#include <string>
#include <vector>

class Config;

class Strategy
{
  public :

  using Movement = std::pair<std::string, int>;
  static inline Movement endMovement{"", 0};

  private :

  class Block
  {
    private :

    enum class EndCondition
    {
      CannotMove
    };

    std::vector<EndCondition> endConditions;
    std::vector<std::tuple<std::string,std::string,std::string,int>> movements;

    private :

    static EndCondition str2condition(const std::string & s);

    public :

    Block(std::vector<std::string> endConditionsStr);
    void addMovement(std::string definition);
    const std::string getInitialState() const;
    bool empty();
    Movement getMovement(const Config & c, const std::string & transition);
    bool isFinished(const Config & c, const Movement & movement);
  };

  private :

  std::string initialState{"UNDEFINED"};
  std::vector<Block> blocks;
  std::size_t currentBlock{0};

  public :

  Strategy(std::vector<std::string> definition);
  Movement getMovement(const Config & c, const std::string & transition);
  const std::string getInitialState() const;
  void reset();
};

#endif
