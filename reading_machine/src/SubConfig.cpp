#include "SubConfig.hpp"

SubConfig::SubConfig(BaseConfig & model, std::size_t spanSize) : model(model), spanSize(spanSize)
{
  rawInput = model.rawInput;
  wordIndex = model.wordIndex;
  characterIndex = model.characterIndex;
  state = model.state;
  history = model.history;
  stack = model.stack;
  extraColumns = model.extraColumns;
  predicted = model.predicted;
  lastPoppedStack = model.lastPoppedStack;
  lastAttached = model.lastAttached;
  currentWordId = model.currentWordId;
  appliableSplitTransitions = model.appliableSplitTransitions;
  appliableTransitions = model.appliableTransitions;
  mcd = model.mcd;
  if (model.strategy.get() != nullptr)
    strategy.reset(new Strategy(model.getStrategy()));
  update();
}

bool SubConfig::needsUpdate()
{
  return wordIndex-firstLineIndex >= 0.8*getNbLines();
}

bool SubConfig::update()
{
  unsigned int currentLastLineIndex = firstLineIndex + getNbLines();

  if (getNbLines() > 0 and currentLastLineIndex >= model.getNbLines()-1)
    return false;

  std::size_t newFirstLineIndex = spanSize/2 >= wordIndex ? 0 : wordIndex - spanSize/2;
  std::size_t newLastLineIndex = std::min(newFirstLineIndex + spanSize, model.getNbLines());
  unsigned int newLineNumber = newLastLineIndex - newFirstLineIndex;

  if (getNbLines() < newLineNumber)
    resizeLines(newLineNumber);

  // The two windows are disjoint, we must copy every value from model
  if (currentLastLineIndex <= newFirstLineIndex or newLastLineIndex <= firstLineIndex)
  {
    auto linesBegin = getIterator(0, firstLineIndex, 0);
    auto firstToSave = model.getConstIterator(0, newFirstLineIndex, 0);
    auto lastToSave = model.getConstIterator(0, newLastLineIndex, 0);

    while (firstToSave != lastToSave)
      *linesBegin++ = *firstToSave++;
  } // The new window is a bit shifted to the right, we will keep the lasts elements from old window
  else if (currentLastLineIndex > newFirstLineIndex and currentLastLineIndex < newLastLineIndex)
  {
    auto linesBegin = getIterator(0, firstLineIndex, 0);
    auto firstToSave = getConstIterator(0, newFirstLineIndex, 0);
    auto lastToSave = getConstIterator(0, currentLastLineIndex, 0);

    while (firstToSave != lastToSave)
      *linesBegin++ = *firstToSave++;

    firstToSave = model.getConstIterator(0, currentLastLineIndex, 0);
    lastToSave = model.getConstIterator(0, newLastLineIndex, 0);

    while (firstToSave != lastToSave)
      *linesBegin++ = *firstToSave++;
  } // If the two windows are the same, do nothing.
  else if (firstLineIndex == newFirstLineIndex and currentLastLineIndex == newLastLineIndex)
  {
  }
  else
  {
    util::myThrow("Update after a regression of wordIndex is not yet supported !");
  }

  if (getNbLines() > newLineNumber)
    resizeLines(newLineNumber);

  firstLineIndex = newFirstLineIndex;

  return true;
}

std::size_t SubConfig::getNbColumns() const
{
  return model.getNbColumns();
}

std::size_t SubConfig::getColIndex(const std::string & colName) const
{
  return model.getColIndex(colName);
}

bool SubConfig::hasColIndex(const std::string & colName) const
{
  return model.hasColIndex(colName);
}

const std::string & SubConfig::getColName(int colIndex) const
{
  return model.getColName(colIndex);
}

std::size_t SubConfig::getFirstLineIndex() const
{
  return firstLineIndex;
}

