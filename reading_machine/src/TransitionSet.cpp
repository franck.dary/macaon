#include "TransitionSet.hpp"
#include <limits>

TransitionSet::TransitionSet(const std::string & filename)
{
  addTransitionsFromFile(filename);
}

TransitionSet::TransitionSet(const std::vector<std::string> & filenames)
{
  for (auto & filename : filenames)
    addTransitionsFromFile(filename);
}

void TransitionSet::addTransitionsFromFile(const std::string & filename)
{
  FILE * file = std::fopen(filename.c_str(), "r");
  if (!file)
    util::myThrow(fmt::format("cannot open file '{}'", filename));

  char readBuffer[1024];

  while (!std::feof(file))
  {
    if (readBuffer != std::fgets(readBuffer, 1024, file))
      break;

    std::string transitionName = readBuffer;
    if (transitionName.back() == '\n')
      transitionName.pop_back();

    transitions.emplace_back(transitionName);
  }

  std::fclose(file);
}

std::vector<std::pair<Transition*, int>> TransitionSet::getAppliableTransitionsCosts(const Config & c, bool dynamic)
{
  using Pair = std::pair<Transition*, int>;
  std::vector<Pair> appliableTransitions;

  auto links = computeLinks(c);

  for (unsigned int i = 0; i < transitions.size(); i++)
    if (transitions[i].appliable(c))
      appliableTransitions.emplace_back(&transitions[i], dynamic ? transitions[i].getCostDynamic(c, links) : transitions[i].getCostStatic(c, links));

  std::sort(appliableTransitions.begin(), appliableTransitions.end(), 
  [](const Pair & a, const Pair & b)
  {
    return a.second < b.second;
  });

  return appliableTransitions;
}

std::vector<Transition *> TransitionSet::getNAppliableTransitions(const Config & c, int n)
{
  std::vector<Transition *> result;

  for (unsigned int i = 0; i < transitions.size(); i++)
    if (transitions[i].appliable(c))
      result.emplace_back(&transitions[i]);

  if ((int)result.size() > n)
    util::myThrow(fmt::format("there are {} appliable transitions n = {}\n", result.size(), n));

  return result;
}

std::vector<int> TransitionSet::getAppliableTransitions(const Config & c)
{
  std::vector<int> result;

  for (unsigned int i = 0; i < transitions.size(); i++)
    if (transitions[i].appliable(c))
      result.emplace_back(1);
    else
      result.emplace_back(0);

  return result;
}

std::map<std::string, int> TransitionSet::computeLinks(const Config & c)
{
  std::map<std::string, int> links{{"StackRight", 0}, {"BufferRight", 0}, {"BufferRightHead", 0}, {"BufferStack", 0}};

  if (c.has(Config::headColName,0,0))
  {
    int nbLinksStackRight = 0;
    int nbLinksBufferRight = 0;
    int nbLinksBufferRightHead = 0;
    int nbLinksBufferStack = 0;
    if (c.hasStack(0))
    {
      if ((std::size_t)std::stoi(c.getConst(Config::headColName, c.getStack(0), 0)) >= c.getWordIndex())
        nbLinksStackRight++;
      auto childs = util::split(c.getConst(Config::childsColName, c.getStack(0), 0), '|');
      for (auto & child : childs)
      {
        if ((std::size_t)std::stoi(child) >= c.getWordIndex())
          nbLinksStackRight++;
      }
    }

    auto head = c.getConst(Config::headColName, c.getWordIndex(), 0);
    if (head != "_" and (std::size_t)std::stoi(c.getConst(Config::headColName, c.getWordIndex(), 0)) > c.getWordIndex())
    {
      nbLinksBufferRight++;
      nbLinksBufferRightHead++;
    }
    auto childs = util::split(c.getConst(Config::childsColName, c.getWordIndex(), 0), '|');
    for (auto & child : childs)
      if ((std::size_t)std::stoi(child) > c.getWordIndex())
        nbLinksBufferRight++;
    auto bufferHead = c.getConst(Config::headColName, c.getWordIndex(), 0);
    for (unsigned int i = 0; i < c.getStackSize(); i++)
    {
      auto stackHead = c.getConst(Config::headColName, c.getStack(i), 0);
      if (bufferHead != "_" and stackHead != "_")
        if ((std::size_t)std::stoi(bufferHead) == c.getStack(i) or (std::size_t)std::stoi(stackHead) == c.getWordIndex())
          nbLinksBufferStack++;
    }

    links.at("StackRight") = nbLinksStackRight;
    links.at("BufferRight") = nbLinksBufferRight;
    links.at("BufferRightHead") = nbLinksBufferRightHead;
    links.at("BufferStack") = nbLinksBufferStack;
  }

  return links;
}

std::vector<Transition *> TransitionSet::getBestAppliableTransitions(const Config & c, const std::vector<int> & appliableTransitions, bool dynamic)
{
  int bestCost = std::numeric_limits<int>::max();
  std::vector<Transition *> result;
  std::vector<int> costs(transitions.size());

  auto links = computeLinks(c);

  for (unsigned int i = 0; i < transitions.size(); i++)
  {
    if (!appliableTransitions[i])
    {
      costs[i] = std::numeric_limits<int>::max();
      continue;
    }

    int cost = dynamic ? transitions[i].getCostDynamic(c, links) : transitions[i].getCostStatic(c, links);

    costs[i] = cost;
    if (cost < bestCost)
      bestCost = cost;
  }

  for (unsigned int i = 0; i < transitions.size(); i++)
    if (costs[i] == bestCost)
      result.emplace_back(&transitions[i]);
    
  return result;
}

std::size_t TransitionSet::size() const
{
  return transitions.size();
}

std::size_t TransitionSet::getTransitionIndex(const Transition * transition) const
{
  if (!transition)
    util::myThrow("transition is null");

  int index = transition - &transitions[0];
  if (index < 0 or index >= (int)transitions.size())
    util::myThrow(fmt::format("transition index '{}' out of bounds [0;{}[", index, transitions.size()));
  return index;
}

Transition * TransitionSet::getTransition(std::size_t index)
{
  return &transitions[index];
}

Transition * TransitionSet::getTransition(const std::string & name)
{
  for (auto & transition : transitions)
    if (transition.getName() == name)
      return &transition;

  return nullptr;
}

